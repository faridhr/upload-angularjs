let module = angular.module('angularApp', []);

module.controller('uploadFilesController', ['$scope', function($scope) {
  
   $scope.path = {
      show : false,
      text : 'ABCDEFG'
   };

   $('.input-file').change(function (e) { 
      handleUploadChange(e);
   });

   $scope.handleShow = function() {
      $scope.path = {
         ...$scope.path,
         show: !$scope.path.show
      };
   };

   let handleUploadChange = (e) => {
      $scope.path = {
        show: true,
        text: e.target.files[0].name
      };
   }
}]);